#/bin/bash
docker pull grafana/grafana:7.5.4 || exit 1
docker pull gongcqq/prometheus-operator:v0.47.0 || exit 1
docker pull gongcqq/kube-rbac-proxy:v0.8.0 || exit 1
docker pull gongcqq/node-exporter:v1.1.2 || exit 1
docker pull gongcqq/prometheus:v2.26.0 || exit 1
docker pull gongcqq/kube-state-metrics:v2.0.0 || exit 1
docker pull gongcqq/k8s-prometheus-adapter:v0.8.4 || exit 1
docker pull gongcqq/blackbox-exporter:v0.18.0 || exit 1
docker pull gongcqq/configmap-reload:v0.5.0 || exit 1
docker pull gongcqq/alertmanager:v0.21.0 || exit 1
docker pull gongcqq/prometheus-config-reloader:v0.47.0 || exit 1

docker tag gongcqq/kube-rbac-proxy:v0.8.0 quay.io/brancz/kube-rbac-proxy:v0.8.0
docker tag gongcqq/node-exporter:v1.1.2 quay.io/prometheus/node-exporter:v1.1.2
docker tag gongcqq/prometheus:v2.26.0 quay.io/prometheus/prometheus:v2.26.0
docker tag gongcqq/alertmanager:v0.21.0 quay.io/prometheus/alertmanager:v0.21.0
docker tag gongcqq/configmap-reload:v0.5.0 jimmidyson/configmap-reload:v0.5.0
docker tag gongcqq/blackbox-exporter:v0.18.0 quay.io/prometheus/blackbox-exporter:v0.18.0
docker tag gongcqq/k8s-prometheus-adapter:v0.8.4 directxman12/k8s-prometheus-adapter:v0.8.4
docker tag gongcqq/kube-state-metrics:v2.0.0 k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.0.0
docker tag gongcqq/prometheus-operator:v0.47.0 quay.io/prometheus-operator/prometheus-operator:v0.47.0
docker tag gongcqq/prometheus-config-reloader:v0.47.0 quay.io/prometheus-operator/prometheus-config-reloader:v0.47.0

docker rmi -f gongcqq/prometheus-operator:v0.47.0
docker rmi -f gongcqq/kube-rbac-proxy:v0.8.0
docker rmi -f gongcqq/node-exporter:v1.1.2
docker rmi -f gongcqq/prometheus:v2.26.0
docker rmi -f gongcqq/alertmanager:v0.21.0
docker rmi -f gongcqq/kube-state-metrics:v2.0.0
docker rmi -f gongcqq/k8s-prometheus-adapter:v0.8.4
docker rmi -f gongcqq/configmap-reload:v0.5.0
docker rmi -f gongcqq/blackbox-exporter:v0.18.0
docker rmi -f gongcqq/prometheus-config-reloader:v0.47.0